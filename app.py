from flask import Flask
from flask_restplus import Resource, Api

app = Flask(__name__)

@app.route('/hello')
def hello():
    return "hello world"

if __name__ == '__main__':
    app.run(port=4322)